package com.example.client

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.Observer
import com.example.client.databinding.ActivityMainBinding
import com.example.pepLib.IService
import com.example.pepLib.IServiceCallback
import com.example.pepLib.getDataTypes.Data
import com.example.pepLib.getIndexTypes.Index
import com.example.pepLib.sensorsTypes.SensorsItem
import com.example.pepLib.stationTypes.StationDataItem

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    private var mStub: IService? = null

    private val callbackStubImp  = object : IServiceCallback.Stub() {

        override fun onCallbackStation(data: MutableList<StationDataItem>?) {
            binding.dataTV.text = data.toString()
            binding.dataTV.requestLayout()
        }

        override fun onCallbackSensors(data: MutableList<SensorsItem>?) {
            binding.dataTV.text = data.toString()
            binding.dataTV.requestLayout()
        }

        override fun onCallbackIndex(data: Index?) {
            binding.dataTV.text = data.toString()
            binding.dataTV.requestLayout()
        }

        override fun onCallbackData(data: Data?) {
            binding.dataTV.text = data.toString()
            binding.dataTV.requestLayout()
        }
    }

    private val serviceConnection  = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            Log.d(TAG, "onServiceConnected")
            mStub = IService.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(className: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected")
            mStub = null
        }
    }

    private lateinit var  binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.getDataBtn.setOnClickListener{ this.getData() }
        binding.getSensorBtn.setOnClickListener{ this.getSensor() }
        binding.getIndexBtn.setOnClickListener{ this.getIndex() }
        binding.getStationBtn.setOnClickListener{ this.getStation() }

        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()
        if (mStub==null) {
            connectServer()
        }
    }

    override fun onDestroy() {
        if (mStub != null) {
            unbindService(serviceConnection)
        }
        super.onDestroy()
    }



    private fun connectServer() {
        val intent = Intent("com.example.server.Service.Action")
        intent.`package` = "com.example.server"
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        val res = bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        Log.d(TAG, res.toString())
    }

    private fun getData() { mStub?.idApiCall(92,0, callbackStubImp) }

    private fun getSensor() { mStub?.idApiCall(14,1, callbackStubImp) }

    private fun getIndex() { mStub?.idApiCall(52,2, callbackStubImp) }

    private fun getStation() { mStub?.baseApiCall(3, callbackStubImp) }
}