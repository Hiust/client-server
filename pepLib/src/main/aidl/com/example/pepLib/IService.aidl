// IService.aidl
package com.example.pepLib;

import com.example.pepLib.IServiceCallback;

interface IService {
    void baseApiCall(int operation, in IServiceCallback callback);
    void idApiCall(int id, int operation, in IServiceCallback callback);
}