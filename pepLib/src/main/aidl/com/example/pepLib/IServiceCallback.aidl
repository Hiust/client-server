// IServiceCallback.aidl
package com.example.pepLib;

import com.example.pepLib.stationTypes.StationDataItem;
import com.example.pepLib.sensorsTypes.SensorsItem;
import com.example.pepLib.getIndexTypes.Index;
import com.example.pepLib.getDataTypes.Data;

interface IServiceCallback {
    void onCallbackStation(in List<StationDataItem> data);
    void onCallbackSensors(in List<SensorsItem> data);
    void onCallbackIndex(in Index data);
    void onCallbackData(in Data data);
}