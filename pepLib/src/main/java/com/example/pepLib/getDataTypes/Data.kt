package com.example.pepLib.getDataTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Data(
    val key: String?,
    val values: List<Value>?
) : Parcelable