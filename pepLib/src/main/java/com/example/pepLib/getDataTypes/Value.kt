package com.example.pepLib.getDataTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Value(
    val date: String?,
    val value: Double?
) : Parcelable