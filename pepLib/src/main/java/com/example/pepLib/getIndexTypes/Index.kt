package com.example.pepLib.getIndexTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Index(
    val id: Int?,
    val no2CalcDate: String?,
    val no2IndexLevel: IndexLevel?,
    val no2SourceDataDate: String?,
    val o3CalcDate: String?,
    val o3IndexLevel: IndexLevel?,
    val o3SourceDataDate: String?,
    val pm10CalcDate: String?,
    val pm10IndexLevel: IndexLevel?,
    val pm10SourceDataDate: String?,
    val pm25CalcDate: String?,
    val pm25IndexLevel: IndexLevel?,
    val pm25SourceDataDate: String?,
    val so2CalcDate: String?,
    val so2IndexLevel: IndexLevel?,
    val so2SourceDataDate: String?,
    val stCalcDate: String?,
    val stIndexCrParam: String?,
    val stIndexLevel: IndexLevel?,
    val stIndexStatus: Boolean?,
    val stSourceDataDate: String?
) : Parcelable