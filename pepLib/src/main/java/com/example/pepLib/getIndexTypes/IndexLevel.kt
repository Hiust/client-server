package com.example.pepLib.getIndexTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IndexLevel(
    val id: Int?,
    val indexLevelName: String?
) : Parcelable