package com.example.pepLib.sensorsTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Param(
    val idParam: Int?,
    val paramCode: String?,
    val paramFormula: String?,
    val paramName: String?
) : Parcelable