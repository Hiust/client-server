package com.example.pepLib.sensorsTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SensorsItem(
    val id: Int?,
    val param: Param?,
    val stationId: Int?
) : Parcelable