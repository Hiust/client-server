package com.example.pepLib.stationTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class City(
    val commune: Commune?,
    val id: Int,
    val name: String?
) : Parcelable