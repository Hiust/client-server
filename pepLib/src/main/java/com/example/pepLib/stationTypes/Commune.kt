package com.example.pepLib.stationTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Commune(
    val communeName: String?,
    val districtName: String?,
    val provinceName: String?
) : Parcelable