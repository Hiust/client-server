package com.example.pepLib.stationTypes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StationDataItem(
    val addressStreet: String?,
    val city: City?,
    val gegrLat: String?,
    val gegrLon: String?,
    val id: Int?,
    val stationName: String?
) : Parcelable