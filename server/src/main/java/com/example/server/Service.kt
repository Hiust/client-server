package com.example.server

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.apicallerapp.data.RetrofitInstance
import com.example.pepLib.IService
import com.example.pepLib.IServiceCallback
import com.example.server.data.network.remote.GiosService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class Service : Service() {

    private val TAG = "Service"

    var api: GiosService? = null

    override fun onCreate() {
        super.onCreate()

    }

    private val binder = object : IService.Stub() {

        override fun baseApiCall(operation: Int, callback: IServiceCallback?) {
            api = RetrofitInstance().create(GiosService::class.java)
            Log.d(TAG, "$operation")
            GlobalScope.launch(Dispatchers.IO) {
                try {
                    val response = api?.getAllStations()
                    val respBody = response?.body()
                    if (respBody != null) {
                        Log.d("Main", respBody.toString())
                        callback?.onCallbackStation(respBody)
                    }
                } catch (e: Exception) {
                    Log.e("Main", "Error: ${e.message}")
                }
            }
        }

        override fun idApiCall(id: Int, operation: Int, callback: IServiceCallback?) {
            api = RetrofitInstance().create(GiosService::class.java)
            Log.d(TAG, "$id + $operation")
            GlobalScope.launch(Dispatchers.IO) {
                try {
                    when (operation) {

                        1 -> {
                            val response = api?.getSensor(id)
                            if (response?.body() != null) {
                                Log.d("Main", response.body().toString())
                                callback?.onCallbackSensors(response.body())
                            }
                        }

                        0 -> {
                            val response = api?.getData(id)
                            if (response?.body() != null) {
                                Log.d("Main", response.body().toString())
                                callback?.onCallbackData(response.body())
                            }
                        }

                        2 -> {
                            val response = api?.getIndex(id)
                            if (response?.body() != null) {
                                Log.d("Main", response.body().toString())
                                callback?.onCallbackIndex(response.body())
                            }
                        }

                    }
                } catch (e: Exception) {
                    Log.e("Main", "Error: ${e.message}")
                }
            }
        }
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind() called with: intent = $intent")
        return binder
    }
}