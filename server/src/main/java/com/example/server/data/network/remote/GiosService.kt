package com.example.server.data.network.remote

import com.example.pepLib.getDataTypes.Data
import com.example.pepLib.getIndexTypes.Index
import com.example.pepLib.sensorsTypes.SensorsItem
import com.example.pepLib.stationTypes.StationDataItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

public interface GiosService {
    @GET("station/findAll")
    suspend fun getAllStations(): Response<List<StationDataItem>>

    @GET("station/sensors/{id}")
    suspend fun getSensor(@Path("id") stationId : Int): Response<List<SensorsItem>>

    @GET("data/getData/{id}")
    suspend fun getData(@Path("id") sensorId : Int): Response<Data>

    @GET("aqindex/getIndex/{id}")
    suspend fun getIndex(@Path("id") stationId : Int): Response<Index>
}